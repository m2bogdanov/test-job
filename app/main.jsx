import React from 'react';
import { render } from 'react-dom';
import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { Provider } from 'react-redux';

import App from './components/App/App';

import '../vendor/bootstrap.css';
import './base.styl';
import reducer from './reducers';
import saga from './sagas';

const sagaMiddleware = createSagaMiddleware();
const enhancers = compose(
  applyMiddleware(sagaMiddleware),
);
const store = createStore(reducer, enhancers);
sagaMiddleware.run(saga);

render(
	<Provider store={store}>
		<App />
	</Provider>,
  document.body.appendChild(document.createElement('div')),
);
