import * as actions from '../actions/score';

const defaultState = {
	list: [],
	loading: false,
};

export default (state = defaultState, action) => {
	switch (action.type) {
		case actions.INDEX_REQUEST: {
			return { ...state, list: [], loading: true };
		}
		case actions.INDEX_SUCCESS: {
			return { ...state, list: action.list, loading: false };
		}
		case actions.INDEX_FAILURE: {
			return { ...state, list: [], loading: false };
		}
		default: {
			return state;
		}
	}
};
