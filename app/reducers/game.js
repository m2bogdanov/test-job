import * as actions from '../actions/game';
import config from '../config';

const createArray = () => {
	const array = new Array(config.size);
	array.fill(null);
	return array;
};

const defaultState = {
	id: null,
	loading: false,
	found: 0,
	steps: 0,
	hold: false,
	cards: createArray(),
};

export default (state = defaultState, action) => {
	switch (action.type) {
		case actions.NEW_GAME_REQUEST: {
			return { ...defaultState, loading: true };
		}
		case actions.NEW_GAME_SUCCESS: {
			const cards = createArray();
			return { ...state, loading: false, id: action.id, found: 0, cards, steps: 0 };
		}
		case actions.SET_CARDS: {
			const { found, cards: openCards, steps } = action;

			const cards = state.cards.map((card, index) => (
				openCards[index] || card
			));

			return { ...state, loading: false, found, cards, steps };
		}
		case actions.CLOSE_CARDS: {
			const { tempCode } = action;
			const cards = state.cards.map(card => (
				card && card.tempCode && (tempCode == null || card.tempCode === tempCode) ? null : card
			));

			return { ...state, cards };
		}
		case actions.MOVE_FAILURE: {
			return { ...state, loading: false };
		}
		default: {
			return state;
		}
	}
};
