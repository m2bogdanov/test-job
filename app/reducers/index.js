import { combineReducers } from 'redux';

import game from './game';
import score from './score';

const combinedReducer = combineReducers({
	game,
	score,
});

export default combinedReducer;
