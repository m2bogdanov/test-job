export const INDEX_REQUEST = 'INDEX_REQUEST';
export const INDEX_SUCCESS = 'INDEX_SUCCESS';
export const INDEX_FAILURE = 'INDEX_FAILURE';

export const SUBMIT_REQUEST = 'SUBMIT_REQUEST';
export const SUBMIT_SUCCESS = 'SUBMIT_SUCCESS';
export const SUBMIT_FAILURE = 'SUBMIT_FAILURE';

export function index() {
	return {
		type: INDEX_REQUEST,
	};
}

export function indexSuccess(list) {
	return {
		type: INDEX_SUCCESS,
		list,
	};
}

export function indexFailure(data) {
	return {
		type: INDEX_FAILURE,
		data,
	};
}

export function submit(id, name) {
	return {
		type: SUBMIT_REQUEST,
		id,
		name,
	};
}

export function submitSuccess() {
	return {
		type: SUBMIT_SUCCESS,
	};
}

export function submitFailure(data) {
	return {
		type: SUBMIT_FAILURE,
		data,
	};
}
