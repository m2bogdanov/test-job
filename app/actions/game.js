export const NEW_GAME_REQUEST = 'NEW_GAME_REQUEST';
export const NEW_GAME_SUCCESS = 'NEW_GAME_SUCCESS';
export const NEW_GAME_FAILURE = 'NEW_GAME_FAILURE';

export const MOVE_REQUEST = 'MOVE_REQUEST';
export const SET_CARDS = 'SET_CARDS';
export const CLOSE_CARDS = 'CLOSE_CARDS';
export const MOVE_FAILURE = 'MOVE_FAILURE';

export function newGame() {
	return {
		type: NEW_GAME_REQUEST,
	};
}

export function newGameSuccess(id) {
	return {
		type: NEW_GAME_SUCCESS,
		id,
	};
}

export function newGameFailure(data) {
	return {
		type: NEW_GAME_FAILURE,
		data,
	};
}

export function move(i1, i2) {
	return {
		type: MOVE_REQUEST,
		i1,
		i2,
	};
}

export function moveFailure(data) {
	return {
		type: MOVE_FAILURE,
		data,
	};
}

export function setCards(cards, found, steps) {
	return {
		type: SET_CARDS,
		cards,
		found,
		steps,
	};
}

export function closeCards(tempCode) {
	return {
		type: CLOSE_CARDS,
		tempCode,
	};
}
