import game from './game';
import score from './score';

export default function* saga() {
	yield [
		game(),
		score(),
	];
}
