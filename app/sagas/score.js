import { takeEvery } from 'redux-saga';
import { put } from 'redux-saga/effects';

import { indexSuccess, INDEX_REQUEST, SUBMIT_REQUEST } from '../actions/score';

function* score() {
	const response = yield fetch('/api/games', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
		},
	});
	const json = yield response.json();
	yield put(indexSuccess(json.games));
}

function* submitRequest(action) {
	const form = new FormData();
	form.append('player', action.name);
	yield fetch(`/api/games/${action.id}`, {
		method: 'POST',
		body: form,
	});
	document.location.reload();
}

export default function* scoreSaga() {
	yield [
		takeEvery(INDEX_REQUEST, score),
		takeEvery(SUBMIT_REQUEST, submitRequest),
	];
}
