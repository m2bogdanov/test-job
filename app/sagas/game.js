import { takeEvery, delay } from 'redux-saga';
import { put, select } from 'redux-saga/effects';
import 'whatwg-fetch';
import R from 'ramda';

import { newGameSuccess, closeCards, setCards, MOVE_REQUEST, NEW_GAME_REQUEST } from '../actions/game';

const getGameId = state => state.game.id;

function* newGame() {
	const response = yield fetch('/api/games', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
	});
	const json = yield response.json();
	yield put(newGameSuccess(json.id));
}

function* move(action) {
	const i1 = action.i1;
	const i2 = action.i2;

	yield put(closeCards());

	const gameId = yield select(getGameId);

	const form = new FormData();
	form.append('i1', i1);
	form.append('i2', i2);
	const response = yield fetch(`/api/games/${gameId}/move`, {
		method: 'POST',
		body: form,
	});
	const json = yield response.json();

	const { found, steps, cards } = json;

	const closeAfterDelay = cards[i1].code !== cards[i2].code;
	let tempCards = null;
	let tempCode = null;
	if (closeAfterDelay) {
		tempCode = Math.random();
		tempCards = R.map(c => Object.assign({}, c, { tempCode }), cards);
	}

	yield put(setCards(
		tempCards || cards,
		found,
		steps,
	));

	if (closeAfterDelay) {
		yield delay(2000);
		yield put(closeCards(tempCode));
	}
}

export default function* gameSaga() {
	yield [
		takeEvery(NEW_GAME_REQUEST, newGame),
		takeEvery(MOVE_REQUEST, move),
	];
}
