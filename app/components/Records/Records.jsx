import React from 'react';

import styles from './styles.styl';

const Records = (props) => {
	const trs = props.records.map(item => (
		/* eslint-disable */
		<tr key={item._id}>
			<td>{item.player}</td>
			<td>{item.steps}</td>
		</tr>
		/* eslint-enable */
		));

	return (
		<div className={styles.root}>
			<h4>{'Score'}</h4>
			<table className={'table table-striped table-bordered'}>
				<thead>
					<tr>
						<td>{'Name'}</td>
						<td>{'Total steps'}</td>
					</tr>
				</thead>
				<tbody>
					{trs}
				</tbody>
			</table>
		</div>
	);
};

Records.propTypes = {
	records: React.PropTypes.arrayOf(React.PropTypes.any).isRequired,
};

export default Records;
