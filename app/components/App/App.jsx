import React from 'react';
import { connect } from 'react-redux';
import styles from './styles.styl';

import config from '../../config';
import Game from '../Game/Game';
import Submit from '../Submit/Submit';
import Records from '../Records/Records';
import { index as scoreIndex } from '../../actions/score';
import { newGame } from '../../actions/game';

class App extends React.Component {
	static propTypes = {
		found: React.PropTypes.number,
		scoreIndex: React.PropTypes.func.isRequired,
		newGame: React.PropTypes.func.isRequired,
		records: React.PropTypes.arrayOf(React.PropTypes.any).isRequired,
	};

	componentDidMount() {
		this.props.scoreIndex();
		this.props.newGame();
	}

	render() {
		const className = `container ${styles.container}`;
		const component = (this.props.found < config.mustFind * 2) ? <Game /> : <Submit />;
		return (
			<div className={className} >
				{ component }
				<Records records={this.props.records} />
			</div>
		);
	}
}

const mapStateToProps = state => ({
	found: state.game.found,
	records: state.score.list,
});

const mapDispatchToProps = dispatch => ({
	scoreIndex: () => {
		dispatch(scoreIndex());
	},
	newGame: () => {
		dispatch(newGame());
	},
});

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(App);
