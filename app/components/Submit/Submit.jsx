import React from 'react';
import { connect } from 'react-redux';
import bind from 'bind-decorator';

import { submit } from '../../actions/score';
import styles from './styles.styl';

class Submit extends React.Component {
	static propTypes = {
		submit: React.PropTypes.func.isRequired,
		steps: React.PropTypes.number,
		id: React.PropTypes.string,
	};

	constructor(props) {
		super(props);
		this.state = {
			name: '',
		};
	}

	@bind
	inputHandler(e) {
		this.setState({ name: e.target.value });
	}

	@bind
	formHandler() {
		this.props.submit(this.props.id, this.state.name);
	}

	render() {
		const inputClassName = `form-control ${styles.input}`;
		const buttonClassName = `btn btn-primary ${styles.button}`;
		return (
			<div className={styles.root} >
				<h4 className={styles.title} >{`Congratulations! Your score is ${this.props.steps}`}</h4>
				<label htmlFor="name">{'Your name'}</label>
				<input className={inputClassName} id="name" value={this.state.name} onChange={this.inputHandler} />
				<button className={buttonClassName} onClick={this.formHandler}>
					{'Submit'}
				</button>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	id: state.game.id,
	steps: state.game.steps,
});

const mapDispatchToProps = dispatch => ({
	submit: (id, name) => {
		dispatch(submit(id, name));
	},
});

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(Submit);
