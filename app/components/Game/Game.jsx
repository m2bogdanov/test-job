import React from 'react';
import bind from 'bind-decorator';
import { connect } from 'react-redux';

import { move, newGame, closeCards } from '../../actions/game';
import styles from './styles.styl';

import Card from '../Card/Card';

class Game extends React.Component {
	static propTypes = {
		cards: React.PropTypes.arrayOf(React.PropTypes.any).isRequired,
		move: React.PropTypes.func.isRequired,
		steps: React.PropTypes.number.isRequired,
		newGame: React.PropTypes.func.isRequired,
		closeCards: React.PropTypes.func.isRequired,
		id: React.PropTypes.string,
	}

	constructor(props) {
		super(props);
		this.state = {
			selected: undefined,
		};
	}

	@bind
	clickCallback(index) {
		if (this.props.cards[index] || !this.props.id) {
			return;
		}
		if (this.state.selected !== undefined) {
			if (this.state.selected !== index) {
				this.props.move(this.state.selected, index);
			}

			this.setState({ selected: undefined });
		} else {
			this.setState({ selected: index });
			this.props.closeCards();
		}
	}

	render() {
		const { cards } = this.props;
		const { selected } = this.state;

		return (
			<div className={styles.root} >
				<button className={`${styles.btn} btn btn-primary`} onClick={this.props.newGame}>
					{'New game'}
				</button>
				<div className={styles.steps}>{`Steps so far: ${this.props.steps}`}</div>
				{
					cards.map((card, index) => (
						<Card
							card={card}
							key={index}
							clickCallback={this.clickCallback}
							index={index}
							selected={selected === index}
						/>
					))
				}
			</div>
		);
	}
}

const mapStateToProps = state => ({
	cards: state.game.cards,
	steps: state.game.steps,
	id: state.game.id,
});

const mapDispatchToProps = dispatch => ({
	move: (i1, i2) => {
		dispatch(move(i1, i2));
	},
	newGame: () => {
		dispatch(newGame());
	},
	closeCards: () => {
		dispatch(closeCards());
	},
});

export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(Game);
