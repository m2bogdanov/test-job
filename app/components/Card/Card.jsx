import React from 'react';
import bind from 'bind-decorator';

import styles from './styles.styl';
import topSrc from './top.svg';

class Card extends React.Component {
	static propTypes = {
		selected: React.PropTypes.bool.isRequired,
		card: React.PropTypes.shape({
			image: React.PropTypes.string,
			images: React.PropTypes.Object,
			value: React.PropTypes.string,
			suit: React.PropTypes.string,
			code: React.PropTypes.string,
		}),
		index: React.PropTypes.number.isRequired,
		clickCallback: React.PropTypes.func.isRequired,
	}

	@bind
	handler(e) {
		e.preventDefault();
		const { clickCallback, index } = this.props;
		clickCallback(index);
	}

	render() {
		const src = this.props.card ? this.props.card.images.png : topSrc;
		let className = styles.img;
		if (this.props.selected) {
			className += ` ${styles.selected}`;
		}
		return (
			<div className={styles.container}>
				<img onClick={this.handler} className={className} src={src} alt="card" />
			</div>
		);
	}
}

export default Card;
