# Memory card game

This is a test job

## Requirements

Install `yarn`, read how [here](https://yarnpkg.com/en/docs/install).

## Installation

Run the following commands:

1. `git clone git@github.com:michael-bogdanov/memory-card-game.git`
2. `cd memory-card-game`
3. `yarn install`

To start the application you can now run: `yarn start`