import Hapi from 'hapi';
import Mongoose from 'mongoose';
import routes from './routes';

Mongoose.Promise = global.Promise;

async function start() {
	await Mongoose.connect('mongodb://test:test@ds117929.mlab.com:17929/memory-game');
	const server = new Hapi.Server();

	server.connection({
		host: 'localhost',
		port: 8000,
	});

	routes(server);

	await server.start();

	// eslint-disable-next-line no-console
	console.log('Server running at:', server.info.uri);
}

start().catch((err) => { throw err; });
