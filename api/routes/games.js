import config from '../../app/config';
import Game from '../models/game';

export function create(server) {
	server.route({
		method: 'POST',
		path: '/games',
		handler: async (request, reply) => {
			const game = new Game();
			await game.save();
			reply({
				id: game._id, // eslint-disable-line no-underscore-dangle
				deck: game.deck,
			});
		},
	});
}

export function move(server) {
	server.route({
		method: 'POST',
		path: '/games/{id}/move',
		handler: async (request, reply) => {
			const { id } = request.params;
			const { i1, i2 } = request.payload;
			const game = await Game.findById(id);
			const { hand, deck } = game;
			await game.move(i1, i2);
			reply({
				cards: {
					[i1]: deck[hand[i1]],
					[i2]: deck[hand[i2]],
				},
				steps: game.steps,
				found: game.foundCount,
			});
		},
	});
}

export function update(server) {
	server.route({
		method: 'POST',
		path: '/games/{id}',
		handler: async (request, reply) => {
			const { id } = request.params;
			const { player } = request.payload;
			const game = await Game.findById(id);
			game.set({ player });
			await game.save();
			reply({ });
		},
	});
}

export function index(server) {
	server.route({
		method: 'GET',
		path: '/games',
		handler: async (request, reply) => {
			const { limit } = request.params;
			const games = await Game.find({ foundCount: { $gte: config.mustFind * 2 } })
				.limit(limit || 5)
				.sort({ steps: 1 })
				.select({ id: 1, player: 1, steps: 1 });
			reply({ games });
		},
	});
}

export default function (server) {
	index(server); // POST games
	create(server); // POST games
	update(server); // POST games/{id}
	move(server);   // POST games/{id}/move
}
