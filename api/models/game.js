// Using old style function definitions because of Mongoose
// passes the current object only via context
/* eslint-disable func-names */

import Mongoose, { Schema } from 'mongoose';
import request from 'request-promise';
import R from 'ramda';

import config from '../../app/config';

const newDeckUrl = `http://deckofcardsapi.com/api/deck/new/draw/?count=${config.size / 2}`;

const gameSchema = new Schema({
	user: { type: String, trim: true },
	deck: { type: Object },
	steps: { type: Number, default: 0 },
	hand: { type: Array },
	found: { type: Array },
	player: { type: String },
	foundCount: { type: Number, default: 0 },
});

const shuffle = R.sortBy(() => Math.random() - 0.5);

gameSchema.pre('save', function (next) {
	const game = this;
	if (game.isNew) {
		request(newDeckUrl).then((data) => {
			const parsedData = JSON.parse(data);
			const deck = parsedData.cards.reduce((acc, card) => (
				{ ...acc, [card.code]: card }
			), {});
			const cardCodes = Object.keys(deck);
			const hand = shuffle(cardCodes.concat(cardCodes));
			const found = R.times(R.F, config.size);
			game.set({
				deck,
				hand,
				found,
			});
			next(game);
		}).catch((err) => {
			throw err;
		});
	} else {
		game.set({
			foundCount: game.found.filter(x => x).length,
		});
		next();
	}
});

gameSchema.methods.move = function (i1, i2) {
	const game = this;
	const { hand, found, steps } = game;
	if (hand[i1] === hand[i2]) {
		game.set({
			found: Object.assign([], found, { [i1]: true, [i2]: true }),
		});
	}
	game.set({
		steps: steps + 1,
	});
	return game.save();
};

const Game = Mongoose.model('Game', gameSchema);

export default Game;
